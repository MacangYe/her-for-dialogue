import ConfigParser
import subprocess

config = ConfigParser.ConfigParser()
config.read("config.cfg")
section = "parameters"

HERUSED = config.getboolean(section,"HERUSED")
prefix = "SampleCombine"

cmd = "./br.sh"
for reshaping in [False,True] :
    for threshold in [0,1,2,3,5,7,10,20] :
        threshold = threshold / 10.0

        config.set(section,"threshold",threshold)
        config.set(section,"HERUSED",HERUSED)
        config.set(section,"reshaping",reshaping)
        
        config.set(section,"recordfile","{}_threshold_{}_reshaping_{}".format(prefix,threshold,reshaping))
        config.write(open("econfig.cfg",'w'))

        subprocess.call(cmd,shell=True)