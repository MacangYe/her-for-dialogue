import matplotlib.pyplot as plt
import numpy as np
import cPickle as pickle
from scipy.interpolate import spline

NOSMOOTH=0
SMOOTH=1

def getDatas(datas,epoch=10,mode=NOSMOOTH):
    pdatas = []    
    if mode == NOSMOOTH :
        i = 0
        temp = []
        for data in datas :
            i += 1
            if i % epoch == 0 :
                pdatas.append(np.mean(temp))
                i = 0
                temp = []
            else:
                temp.append(data)
    return pdatas

def read(filename,epoch=10,mode=NOSMOOTH):
    datas = pickle.load(open(filename,'r'))
    Data = {}
    for item in datas :
        pdatas = getDatas(datas[item],epoch)
        Data[item] = pdatas

    return Data

def paint(data,pname,mode=NOSMOOTH):
    datalength = len(data)
    if mode == NOSMOOTH :
        plt.plot(range(datalength),data,label=pname)
    else:
        x = np.linspace(0,datalength,10*datalength)
        y = spline(range(datalength),data,x)
        plt.plot(x[:-10],y[:-10],label=pname)

SR = "SR"
RW = "RW"
mode = NOSMOOTH
epoch = 100


data1 = read("rcTrue_combineusedkaiguan00.txt",epoch)
data2 = read("SampleCombine_threshold_0.1",epoch)

paint(data1[SR],"data1",mode)
paint(data_1[SR],"data_1",mode)
paint(data2[SR],"data2",mode)



plt.legend()
plt.show()

    
        
