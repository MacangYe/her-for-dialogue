#!/bin/bash

# python run.py --agt 5 --usr 1 --max_turn 40 \
# 	      --episodes 1 \
# 	      --movie_kb_path ./deep_dialog/data/movie_kb.1k.p \
# 	      --goal_file_path ./deep_dialog/data/user_goals_first_turn_template.part.movie.v1.p \
# 	      --intent_err_prob 0.00 \
# 	      --slot_err_prob 0.00 \
# 	      --act_level 0 \
# 		  --run_mode 0

python run.py --agt 9 --usr 1 --max_turn 40 \
	      --movie_kb_path ./deep_dialog/data/movie_kb.1k.p \
	      --dqn_hidden_size 80 \
	      --experience_replay_pool_size 1000 \
	      --episodes 500 \
	      --simulation_epoch_size 100 \
	      --write_model_dir ./deep_dialog/checkpoints/rl_agent_1/ \
	      --run_mode 3 \
	      --act_level 0 \
	      --slot_err_prob 0.00 \
	      --intent_err_prob 0.00 \
	      --batch_size 16 \
	      --goal_file_path ./deep_dialog/data/user_goals_first_turn_template.part.movie.v1.p \
	      --warm_start 1 \
	      --warm_start_epochs 120 \

#test
# python run.py --agt 9 --usr 1 --max_turn 40 \
# 	      --movie_kb_path ./deep_dialog/data/movie_kb.1k.p \
# 	      --dqn_hidden_size 80 \
# 	      --experience_replay_pool_size 1000 \
# 	      --episodes 300  \
# 	      --simulation_epoch_size 100 \
# 	      --write_model_dir ./deep_dialog/checkpoints/rl_agent/ \
# 	      --slot_err_prob 0.00 \
# 	      --intent_err_prob 0.00 \
# 	      --batch_size 16 \
# 	      --goal_file_path ./deep_dialog/data/user_goals_first_turn_template.part.movie.v1.p \
# 	      --trained_model_path ./deep_dialog/checkpoints/rl_agent/noe2e/agt_9_478_500_0.98000.p \
# 	      --run_mode 3