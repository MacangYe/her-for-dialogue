import ConfigParser
import subprocess

config = ConfigParser.ConfigParser()
config.read("config.cfg")
section = "parameters"

HERUSED = config.getboolean(section,"HERUSED")
prefix = "sampleOne"
isSampleSearch = False
isreshaping = config.getboolean(section,"RewardShaping")

cmd = "./br.sh"

if isreshaping :
    for i in range(3) :
        config.set(section,"HERUSED",HERUSED)
        config.set(section,"recordfile","{}_RewardShaping_{}".format(prefix,i))
        config.set(section,"isSampleSearch",isSampleSearch)
        config.set(section,"RewardShaping",True)
        config.write(open("econfig.cfg",'w'))

        subprocess.call(cmd,shell=True)

for num in range(1,10) :
    config.set(section,"sampleNumInSubGoal",num)
    config.set(section,"HERUSED",HERUSED)
    config.set(section,"recordfile","{}_sampleWhole_{}_{}".format(prefix,num,HERUSED))
    config.set(section,"RewardShaping",isreshaping)
    config.set(section,"isSampleSearch",isSampleSearch)    
    config.write(open("econfig.cfg",'w'))

    subprocess.call(cmd,shell=True)

